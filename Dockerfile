FROM python:3

ADD scripts /scripts

RUN pip install -r /scripts/requirements.txt

CMD [ "python", "./scripts/main.py" ]