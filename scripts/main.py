#!/usr/bin/python
# -*- coding:utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging, os

#Loggingsetup
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

updater = Updater(token=os.environ['THEMOMBOT_TG_TOKEN'], use_context=True)
dispatcher = updater.dispatcher
bidsFile = "/csvOutput/bids.csv"
pwd = os.environ['THEMOMBOT_PASSWORD']
adminpwd = os.environ['THEMOMBOT_ADMIN_PASSWORD']
users = []
goalBids = float(os.environ['THEMOMBOT_GOAL_BIDS'])
goalBidsK = float(os.environ['THEMOMBOT_GOAL_BIDS_K'])
currentBids = 0
currentBidsK = 0

## read existing bids from file
try:
    f = open(bidsFile, "r")
    for line in f:
        cells = line.split(';')
        if cells[3] == "KAUTION":
            currentBidsK += float(cells[2])
        else:
            currentBids += float(cells[2])
    f.close();
except:
    print('no savefile found, starting by 0')
    
        



def start(update, context):
    try:
        context.args[0]
    except:
        context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /start PASSWORT eingeben.")
    else:
        if context.args[0] == pwd:
            users.append(str(update.message.from_user.id))
            context.bot.send_message(chat_id=update.message.chat_id, text="Erfolgreich eingeloggt.")
        else:
            context.bot.send_message(chat_id=update.message.chat_id, text="Falsches Passwort. Bitte /start PASSWORT eingeben.")
        
        
def AddBid(update, context):
    global users
    global currentBids
    global currentBidsK
    global goalBids
    global goalBidsK
    if str(update.message.from_user.id) in users:
        if float(context.args[0]) > 0:
            bidString = (str(update.message.from_user.id), str(update.message.from_user.name), str(context.args[0]), "\n")
            f = open(bidsFile, "a")
            f.write(";".join(bidString))
            f.close()
            currentBids += float(context.args[0])
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Du hast",str(context.args[0]),"Euro geboten.")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es befinden sich aktuell",str(round(currentBids, 2)),"Euro im Topf für die Miete.")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es werden noch",str(round(goalBids - currentBids, 2)),"Euro benötigt, um die Miete zu zahlen.")))
            #context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es sind also schon",str(round(currentBids*100/goalBids)),"Prozent der Miete gezahlt")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Außerdem müssen noch",str(round(goalBidsK - currentBidsK, 2)),"Euro für die Kaution gesammelt werden")))
        else:
            context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /bid EURO.CENT eingeben.")
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /start PASSWORT eingeben.")
        
def AddBidK(update, context):
    global users
    global currentBids
    global currentBidsK
    global goalBids
    global goalBidsK
    if str(update.message.from_user.id) in users:
        if float(context.args[0]) > 0:
            bidString = (str(update.message.from_user.id), str(update.message.from_user.name), str(context.args[0]), "KAUTION", "\n")
            f = open(bidsFile, "a")
            f.write(";".join(bidString))
            f.close()
            currentBidsK += float(context.args[0])
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Du hast",str(context.args[0]),"Euro für die Kaution geboten.")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es befinden sich aktuell",str(round(currentBidsK, 2)),"Euro im Topf für die Kaution.")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es werden noch",str(round(goalBidsK - currentBidsK, 2)),"Euro benötigt, um die Kaution zu zahlen.")))
            context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Außerdem müssen noch",str(round(goalBids - currentBids)),"Euro für die Miete gesammelt werden")))
        else:
            context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /bidK EURO.CENT eingeben.")
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /start PASSWORT eingeben.")
        
        
        
def statusBid(update, context):
    global users
    global currentBids
    global currentBidsK
    global goalBids
    global goalBidsK
    if str(update.message.from_user.id) in users:
        context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es werden noch",str(round(goalBids - currentBids, 2)),"Euro benötigt, um die Miete zu zahlen.")))
        context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es sind also schon",str(round(currentBids*100/goalBids, 2)),"Prozent der Miete gezahlt")))
        context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es werden noch",str(round(goalBidsK - currentBidsK, 2)),"Euro benötigt, um die Kaution zu zahlen.")))
        context.bot.send_message(chat_id=update.message.chat_id, text=" ".join(("Es sind also schon",str(round(currentBidsK*100/goalBidsK, 2)),"Prozent der Kaution gezahlt")))
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /start PASSWORT eingeben.")
    
def csvSend(update, context):
    if str(update.message.from_user.id) in users:
        try:
            context.args[0]
        except:
            context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /csv PASSWORT eingeben.")
        else:
            if context.args[0] == adminpwd:
                context.bot.send_document(chat_id=update.message.chat_id, document=open(bidsFile, "rb"))
            else:
                context.bot.send_message(chat_id=update.message.chat_id, text="Falsches Passwort. Bitte /csv PASSWORT eingeben.")
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Bitte /start PASSWORT eingeben.")
    
    

###DISPATCHER###
"""
#Handler für Textnachrichten
txt_handler = MessageHandler(Filters.text, writeQtxt)
dispatcher.add_handler(txt_handler)
"""

#Handler
bid_handler = CommandHandler('bid', AddBid)
dispatcher.add_handler(bid_handler)

bidK_handler = CommandHandler('bidK', AddBidK)
dispatcher.add_handler(bidK_handler)

status_handler = CommandHandler('status', statusBid)
dispatcher.add_handler(status_handler)

start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

csv_handler = CommandHandler('csv', csvSend)
dispatcher.add_handler(csv_handler)


###Start TG###

updater.start_polling()
updater.idle()

